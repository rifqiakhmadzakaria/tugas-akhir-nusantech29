import { Button } from 'antd'
import BreadCrumb from 'components/BreadCrumbs/BreadCrumbs'
import CardStudyCase from 'components/Cards/CardStudyCase'
import NavbarSlide from 'components/NavbarSlide/NavbarSlide'
import PaginationComp from 'components/Pagination/Pagination'
import Link from 'next/link'
import React, { useState } from 'react'
import { studyCaseData } from 'data/studyCaseData'

const itemsPerPage = 9

function OurWorks() {
  const [currentPage, setCurrentPage] = useState(1)
  const totalPages = Math.ceil(studyCaseData.length / itemsPerPage)

  const startIndex = (currentPage - 1) * itemsPerPage
  const endIndex = startIndex + itemsPerPage

  const dataStudy = studyCaseData.slice(startIndex, endIndex)

  const goToPage = (page) => {
    setCurrentPage(page)
  }

  const breadcrumbItems = [{ text: 'Our Works' }]
  const vector = {
    backgroundImage: "url('./static/images/vectors/Ellipse.svg')",
  }

  return (
    <>
      <div
        className="absolute top-0 -left-40 w-[714px] bg-no-repeat bg-contain h-[714px] -z-[999]"
        style={vector}
      />
      <div className="mx-[80px]">
        <div className="mt-[113px] flex justify-between mb-[180px]">
          <div className="w-[619px]">
            <BreadCrumb items={breadcrumbItems} />
            <h1 className="text-[42px] w-[619px] h-[189px] font-bold leading-[1.5] mb-3">
              Hello, We are Nusantech a startup company that help you to fulfill
              your digital needs
            </h1>
            <h3 className="mb-10 text-[24px] font-normal">
              We develop digital strategies, product and services.
            </h3>
            <Button className="text text-[#DF1E36] border-[#DF1E36] text-[16px] font-semibold text-center items-center flex justify-center px-[32px] h-[52px] gap-2 rounded-lg">
              Consult Now
              <img
                src="./static/images/icons/whatsapp.svg"
                alt="logo-whatsapp"
                className=""
              />
            </Button>
          </div>
          <div className="w-[477px] relative flex justify-center">
            <img
              src="./static/images/content/ourWorks/hero-portfolio.svg"
              alt="statiska"
              className="w-[477px]"
            />
            <img
              src="./static/images/icons/frame-polcadot.svg"
              alt="frame-polcadot"
              className="w-[102px]"
            />
          </div>
        </div>
        <div className="ml-14">
          <NavbarSlide />
        </div>
        <div>
          <div className="px-[40px] w-full m-auto mt-[55px]">
            <div className="flex flex-wrap justify-center gap-[40px]">
              {dataStudy.map((e, index) => (
                <div key={index}>
                  <Link href="/ourworkdetail">
                    <CardStudyCase
                      image={e.image}
                      nameCompany={e.nameCompany}
                      title={e.title}
                      webIcon={e.webIcon}
                      androidIcon={e.androidIocn}
                      iosIcon={e.iosIcon}
                    />
                  </Link>
                </div>
              ))}
            </div>
          </div>
          <div className="flex justify-center mt-[60px] mb-[124px]">
            <PaginationComp
              currentPage={currentPage}
              totalPages={totalPages}
              onPageChange={goToPage}
              itemsPerPage={itemsPerPage}
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default OurWorks
