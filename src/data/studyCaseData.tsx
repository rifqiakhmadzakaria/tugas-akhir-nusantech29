// eslint-disable-next-line import/prefer-default-export
export const studyCaseData = [
  {
    image: './static/images/content/trimegah.png',
    nameCompany: 'Trimegah',
    title: 'Trimegah Equity Dashboard',
    webIcon: './static/images/icons/globe.svg',
  },
  {
    image: './static/images/content/pupr.png',
    nameCompany: 'Kementrian PUPR',
    title: 'Trimegah Equity Dashboard',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/beyond.png',
    nameCompany: 'Beyond Run',
    title: 'Trimegah Equity Dashboard',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/kemenkesehatan.png',
    nameCompany: 'Kementerian Kesehatan',
    title: 'Learning Management System',
    webIcon: './static/images/icons/globe.svg',
  },
  {
    image: './static/images/content/astra.png',
    nameCompany: 'Astra International',
    title: 'Astra Green Company',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/shell.png',
    nameCompany: 'Shell Indonesia',
    title: 'Lubeanalyst Tracker',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/agriculture.svg',
    nameCompany: 'Kementerian Pertanian',
    title: 'Sistem Perizinan Pupuk dan Pestisida',
    webIcon: './static/images/icons/globe.svg',
  },
  {
    image: './static/images/content/sipencatar.svg',
    nameCompany: 'Kementerian Perhubungan',
    title: 'SIPENCATAR (Seleksi Penerimaan Calon Taruna/Taruni)',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/pertamina.svg',
    nameCompany: 'Pertamina',
    title: 'Pertamina E- Logistic RU IVr',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/pertamina.svg',
    nameCompany: 'Pertamina',
    title: 'Pertamina E- Logistic RU IVr',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/pertamina.svg',
    nameCompany: 'Pertamina',
    title: 'Pertamina E- Logistic RU IVr',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
  {
    image: './static/images/content/pertamina.svg',
    nameCompany: 'Pertamina',
    title: 'Pertamina E- Logistic RU IVr',
    webIcon: './static/images/icons/globe.svg',
    androidIocn: './static/images/icons/android.svg',
    iosIcon: './static/images/icons/apple.svg',
  },
]
